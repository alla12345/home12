//Задание 1
const a = null >= 0;
console.log('Задание 1 вариант 1', a);
const ab = 5 > 1;
console.log('Задание 1 вариант 2', ab);
const ac = !!'allString';
console.log('Задание 1 вариант 3', ac);

//Задание 2
let a2 = 15;
let a3 = 'mama';
const b2 = a2;

a2 = a3;
a3 = b2;
console.log('Задание 2', a2, a3);

//Задание 3
let c = 'notNumber' / 2;
console.log('Задание 3', c);

//Задание 4
let d2 = 5;
let d3;
let d = (((d2 + 2)**3) / 6) + 4.5;
d3 = d - Math.floor(d);
console.log('Задание 4 остаток', d3);
console.log('Задание 4 результат', d);

//Задание 5
let z;
let big ='55';
z = Number(big);
console.log ('Задание 5', z);

//Задание 6
let v = !!z > d3;
console.log ('Задание 6', v);
